# light-riders

A bot to compete on riddles.io.  This one is in python3.

Helpful links to research maybe:

* https://www.codingame.com/multiplayer/bot-programming/tron-battle
* http://www.sifflez.org/misc/tronbot/index.html
* https://www.a1k0n.net/2010/03/04/google-ai-postmortem.html
* https://www.eecs.wsu.edu/~holder/courses/CptS223/spr08/slides/graphapps.pdf
* https://tonypoer.io/2016/10/28/implementing-minimax-and-alpha-beta-pruning-using-python/
