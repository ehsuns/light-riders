import random
import sys
import json
import numpy

class Vertex:
    def __init__(self):
        self.row = None
        self.col = None
        self.visited = False
        self.num = None
        self.low = None
        self.parent = None


class Bot:


    def __init__(self):
        self.game = None
        self.last_move = None
        self.target_move = [ None, None ]
        self.n_turn = 0
        self.field = None

    def setup(self, game):
        self.game = game


    def do_turn(self):
        self.n_turn += 1

        def flatten(m):
            arr = []
            for r in m:
                arr.append([c[0] for c in r])
            return arr
        
        self.field = flatten(self.game.field.cell)

        legal = self.game.field.legal_moves(self.game.my_botid, self.game.players)
        if len(legal) == 0:
            self.game.issue_order_pass()
        else:
            # chosen = self.choose_longest_path_v2(legal)
            # chosen, _ = self.choose_longest_path_v3(self.game.my_player().row, self.game.my_player().col, 5, legal)
            leaf = self.minimax(legal)
            chosen = leaf[0]
            self.last_move = chosen

            self.log("Moving {}".format(chosen))
            self.game.issue_order(chosen)


    def minimax(self, legal):
        leaf = [None, None, None]
        best_score = [0, 0, 0]
        best_grade = 0
        tmp_grade = 0
        curr_row = self.game.my_player().row
        curr_col = self.game.my_player().col
        for ((row, col), d) in legal:
            m = numpy.matrix(self.field)
            # give a sub-matrix depending on direction heading
            self.log("grade before: %d" % tmp_grade)
            tmp_grade = self.evaluate_position(d, m, curr_row, curr_col, row, col)
            self.log("grade after: %d" % tmp_grade)
            enemy_legal = self.game.field.legal_moves(self.game.other_botid, self.game.players)
            for ((e_row, e_col), e_d) in enemy_legal:
                tmp_grade += self.evaluate_position(d, m, curr_row, curr_col, e_row, e_col)
                next_legal = self.find_legals_for_cell(row, col, e_d)
                for ((row1, col1), d2) in next_legal:
                    tmp_grade += self.evaluate_position(d2, m, curr_row, curr_col, row1, col1)
                    if tmp_grade > best_grade:
                        best_grade = tmp_grade
                        leaf = [ d, e_d, d2 ]

        self.log("CHOSE LEAF: {} with score: {}".format(leaf, best_grade))
        return leaf


    def evaluate_position(self, d, m, curr_row, curr_col, row, col):
        grade = 0
        if d == "up":
            self.log("==================up=======================")
            grade += self.find_area(m[:curr_row, :], curr_row-1, col)
        elif d == "down":
            self.log("=======================down=======================")
            grade += self.find_area(m[curr_row+1:, :], 0, col)
        elif d == "left":
            self.log("=======================left=======================")
            grade += self.find_area(m[:, :curr_col], row, curr_col-1)
        else:
            self.log("=======================right=======================")
            grade += self.find_area(m[:, curr_col + 1:], row, curr_col+1)

        return grade


    def find_target(self, legal):
        curr_row = self.game.my_player().row
        curr_col = self.game.my_player().col

        if curr_row > self.game.field_height // 2:
            self.target_move[0] = -1
        else:
            self.target_move[0] = 1

        if curr_col < self.game.field_width // 2:
            self.target_move[1] = 1
        else:
            self.target_move[1] = -1


    def move_out(self, legal):
        return None


    def spiral_in(self, legal):
        return None


    def check_adj_col(self):
        return None


    def check_adj_row(self):
        return None


    def choose_longest_path_v3(self, start_row, start_col, max_recursions, legal):
        curr_col = start_col
        curr_row = start_row
        max_recursions -= 1
        dir_to_choose = None
        max_steps = 0

        for ((row, col), d) in legal:
            next_col = curr_col + col
            next_row = curr_row + row
            # self.log("{5}.{0}.begin -- col: {1}, row: {2} -- max_steps: {3}, direction: {4}".format(max_recursions, next_col, next_row, max_steps, d, self.n_turn))

            next_d = None
            r_steps = 0
            if max_recursions != 0:
                new_legals = self.find_legals_for_cell(next_row, next_col, d)
                next_d, r_steps = self.choose_longest_path_v3(next_row, next_col, max_recursions, new_legals)

            steps = self.traverse_direction(next_row, next_col, row, col)

            total_steps = steps + r_steps

            if total_steps > max_steps:
                # self.log("{5}.{0}.end -- col: {1}, row: {2} -- steps: {3}, direction: {4}".format(max_recursions, next_col, next_row, total_steps, d, self.n_turn))
                max_steps = total_steps
                dir_to_choose = d

        return dir_to_choose, max_steps


    def choose_longest_path_v2(self, legal):
        curr_col = self.game.my_player().col
        curr_row = self.game.my_player().row

        dir_to_choose = None
        max_steps = 0
        for ((row, col), d) in legal:
            next_col = curr_col + col
            next_row = curr_row + row

            steps = self.traverse_direction(next_row, next_col, row, col)

            if steps > max_steps:
                max_steps = steps
                dir_to_choose = d

        return dir_to_choose
    

    def traverse_direction(self, start_row, start_col, row, col):
        next_row = start_row
        next_col = start_col
        next_cell_val = self.game.field.cell[next_row][next_col][0] if self.is_legal(next_row, next_col) else -1
        steps = 0
        while (next_cell_val == 2) and self.is_legal(next_row, next_col):
            steps += 1
            next_col += col
            next_row += row
            next_cell_val = self.game.field.cell[next_row][next_col][0] if self.is_legal(next_row, next_col) else -1   

        return steps


    def is_legal_for_enemy(self, row, col):
        return self.game.field.is_legal(row,col, self.game.other_botid)


    def is_legal(self, row, col):
        return self.game.field.is_legal(row, col, self.game.my_botid)


    def find_legals_for_cell(self, row, col, direction):
        legals = []
        if self.game.field.is_legal(row + 1, col, self.game.my_botid) and direction != "up":
            legals.append( ((1, 0), "down"))
        if self.game.field.is_legal(row - 1, col, self.game.my_botid) and direction != "down":
            legals.append(((-1, 0), "up"))
        if self.game.field.is_legal(row, col + 1, self.game.my_botid) and direction != "left":
            legals.append(((0, 1), "right"))
        if self.game.field.is_legal(row, col - 1, self.game.my_botid) and direction != "right":
            legals.append(((0, -1), "left"))

        return legals


    def log(self, logline):
        sys.stderr.write(logline + "\n")
        sys.stderr.flush()


    def find_area(self, m, row, col):
        # self.log("{}".format(m))
        # m = numpy.matrix(self.field)
        (h, w) = m.shape
        # self.log("w: {} h: {}".format(w, h))
        # self.log("r: {} c: {}".format(row, col))

        if row < 0 or col < 0 or row >= h or col >= w: # case out of bound
            return 0

        # self.log("cell:{}".format(m[row, col]))
        if m[row, col] == 2: # if is empty cell, recurse and increment area by 1
            m[row, col] = -1 # set as "visited"
            return self.find_area(m, row + 1, col) + self.find_area(m, row - 1, col) + self.find_area(m, row , col + 1) + self.find_area(m, row , col - 1) + 1
        else:
            return 0